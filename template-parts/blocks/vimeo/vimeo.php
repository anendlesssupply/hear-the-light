<?php
$id = 'wp-block-acf-vimeo-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}
$className = 'vimeo-container';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
$data_id = get_field('vimeo_id') ? get_field('vimeo_id') : '';
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="vimeo" data-id="<?php echo $data_id; ?>"></div>
</div>