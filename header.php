<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Hear_the_Light
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<?php 
$bg_style = "";
$site_bg_image = get_field('site_background_image', 'option'); 
if($site_bg_image):
	$site_bg_src = $site_bg_image['sizes']['medium'];
	$bg_style = "background-image:url(" . $site_bg_src . "); background-size:cover;background-repeat:no-repeat;background-position:center;";
endif; ?>
<body <?php body_class(); ?> style="<?php echo $bg_style; ?>">
<?php locate_template("assets/svg/symbol-defs.svg", TRUE, TRUE); ?>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'hear-the-light' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding screen-reader-text">
			<?php
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
				<?php
			else :
				?>
				<p class="site-title"><?php bloginfo( 'name' ); ?></p>
				<?php
			endif; ?>
		</div><!-- .site-branding -->

		<button type="button" class="shuffle" title="<?php echo get_field('shuffle_button_label', 'option'); ?>">
			<span class="screen-reader-text"><?php echo get_field('shuffle_button_label', 'option'); ?></span>
			<svg width="100%" height="100%" viewBox="0 0 36 36">
				<path d="M17.0000804,17.4889243 C17.2483534,17.414118 17.4343218,17.2307659 17.4929354,16.9960925 C17.5629433,16.5790609 17.3142945,16.2332416 17.0000865,16.0343925 C16.5039345,15.7496946 15.9256955,15.7404461 15.4358648,15.9247315 C14.7323923,16.2236658 14.2491679,16.8244919 14.0303138,17.4889183 C13.7511976,18.3575271 13.9019712,19.3380756 14.3644435,20.1245024 C14.9243487,21.0766266 15.9199984,21.7424469 17.0000865,21.9737809 C18.269331,22.2456 19.6370022,21.9305596 20.7071507,21.1958997 C21.9389397,20.3502001 22.7736277,18.965832 23.0002395,17.4889183 C23.2548157,15.8298221 22.7522211,14.08915 21.7356618,12.7534488 C20.6041784,11.2666562 18.8524368,10.2784106 17.0000865,10.0343492 C14.951004,9.76436052 12.8141616,10.3999504 11.19309,11.6820514 C9.4180144,13.0859953 8.27253065,15.2387899 8.03016083,17.4889183 C7.76669351,19.9352896 8.56199257,22.4641461 10.1216687,24.3671218 C11.8064995,26.4228253 14.3532311,27.7310113 17.0000259,27.9737393 C19.8432499,28.234522 22.7638698,27.275131 24.9499256,25.4384586 C27.2881064,23.4739069 28.7674168,20.533837 29.0004531,17.4887365 C29.2477927,14.2568475 28.1004301,10.960837 25.9784973,8.51052625 C23.7321369,5.91657865 20.4225374,4.28342197 17.0000865,4.03408785 C13.3788427,3.77023853 9.68474847,5.05063652 6.95013329,7.43912892 C4.06690826,9.95750052 2.27613533,13.6682395 2.02982602,17.4888577 C1.77073457,21.5066886 3.21179555,25.5937318 5.87877264,28.6094989 C8.67702581,31.7741756 12.782585,33.7291515 16.9996623,33.9735159 C21.4137142,34.2292439 25.8895859,32.6207237 29.1920943,29.6808962 C32.6453339,26.6068259 34.7888431,22.1069329 35,17.4887365"></path>
			</svg>
		</button>
		<button type="button" class="about-link" title="<?php echo get_field('next_button_label', 'option'); ?>">
			<span class="screen-reader-text"><?php echo get_field('next_button_label', 'option'); ?></span>
		</button>
	</header><!-- #masthead -->
