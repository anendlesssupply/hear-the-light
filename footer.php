<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Hear_the_Light
 */

?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
