<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Hear_the_Light
 */

$frontpage_id = get_option( 'page_on_front' );
wp_redirect ( get_permalink ($frontpage_id) );
