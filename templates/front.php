<?php
/**
 * Template Name: Front Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fulfilment_Services_Ltd
 */

get_header();
?>

<?php
while ( have_posts() ) :
	the_post(); ?>

	<div class="intro-video-wrapper">
		<div class="intro-video">
		</div>
		<div class="intro-buttons">
			<button class="enter text-button" type="button">
				<?php echo get_field('enter_button_label', 'option'); ?>
			</button>
			<?php if(get_field('show_unmute_button_on_front_page', 'option')): ?>
				<button class="unmute text-button" type="button">
					<?php echo get_field('unmute_button_label', 'option'); ?>
				</button>
			<?php endif; ?>
		</div>
	</div>

	<?php $main_panels = get_field('main_panels'); 
	if($main_panels):
		foreach($main_panels as $panel_id): 
			if(get_field('more_info', $panel_id)): ?>
				<div class="info-panel info-panel--fixed" data-id="<?php echo $panel_id; ?>">
					<div class="handle">

						<div class="handle-inner"></div>
					</div>
					<button type="button" class="close-text" title="Close panel">
						<span class="screen-reader-text">Close</span>
						<svg width="100%" height="100%" viewBox="0 0 24 24">
							<path d="M18.984 6.422l-5.578 5.578 5.578 5.578-1.406 1.406-5.578-5.578-5.578 5.578-1.406-1.406 5.578-5.578-5.578-5.578 1.406-1.406 5.578 5.578 5.578-5.578z"></path>
						</svg>
					</button>
					<div class="info-panel-inner">
						<?php echo get_field('more_info', $panel_id); ?>
					</div>
				</div>
			<?php endif;
		endforeach;
	endif; ?>

	<div class="swiper-container">
		<!-- Additional required wrapper -->
		<div class="swiper-wrapper">
			<!-- Slides -->
			<?php $intro_panel_title = get_field('intro_panel_title'); 
			if($intro_panel_title): 
				$style = "";
				$text_colour = get_field('text_colour');
				if($text_colour):
					$style .= " --colorText: " . $text_colour . ";";
				endif;
				?>
				<div class="swiper-slide swiper-slide--intro" style="<?php echo $style; ?>" tabindex="0">
					<div class="swiper-slide-inner">
						<h2><?php echo $intro_panel_title; ?></h2>
						<p class="scroll-prompt scroll-prompt--desktop"><?php echo get_field('desktop_scroll_message', 'option'); ?></p>
						<p class="scroll-prompt scroll-prompt--touch"><?php echo get_field('mobile_scroll_message', 'option'); ?></p>
					</div>
				</div>
			<?php endif; ?>

			<?php $main_panels = get_field('main_panels'); 
			if($main_panels):
				foreach($main_panels as $panel_id): 
					$style = "";
					$background_colour = get_field('background_colour', $panel_id); 
					$text_colour = get_field('text_colour', $panel_id); 
					$link_colour = get_field('link_colour', $panel_id); 
					$link_hover_colour = get_field('link_hover_colour', $panel_id);
					if($background_colour):
						$rgb_bg_value = hex2rgb($background_colour) ?: "";
						$rgba_bg_string = $rgb_bg_value ? "rgba(" . $rgb_bg_value . ", 50%)" : $background_colour;
						$style .= " --panelBackground: " . $rgba_bg_string . ";";
					endif;
					if($text_colour):
						$style .= " --colorText: " . $text_colour . ";";
					endif;
					if($link_colour):
						$style .= " --colorLink: " . $link_colour . ";";
					endif;
					if($link_hover_colour):
						$style .= " --colorLinkHover: " . $link_hover_colour . ";";
					endif;
					?>
					<div class="swiper-slide swiper-slide--work" style="<?php echo $style; ?>" tabindex="0">
						<div class="swiper-slide-inner">
							<header class="swiper-slide-header">
								<h2><?php echo get_the_title($panel_id); ?></h2>
								<?php if(get_field('more_info', $panel_id)): ?>
									<button class="text-button" type="button" data-id="<?php echo $panel_id; ?>">
										<span class="screen-reader-text">More info</span>
										<svg aria-hidden="true" viewBox="0 0 24 24">
											<path d="M14 2H4v20h16V8l-6-6zm2 16H8v-2h8v2zm0-4H8v-2h8v2zm-3-5V3.5L18.5 9H13z"/>
										</svg>
									</button>
								<?php endif; ?>
							</header>
							<?php 
							$content_post = get_post($panel_id);
							$content = $content_post->post_content;
							$content = apply_filters('the_content', $content);
							$content = str_replace(']]>', ']]&gt;', $content);
							echo $content; 
							wp_reset_postdata(); ?>
						</div>
					</div>
				<?php endforeach;
			endif; ?>

			<?php $about_id = get_field('about_panel'); 
			if($about_id): 
				$style = "";
				$background_colour = get_field('background_colour', $about_id); 
				$text_colour = get_field('text_colour', $about_id); 
				$link_colour = get_field('link_colour', $about_id); 
				$link_hover_colour = get_field('link_hover_colour', $about_id);
				if($background_colour):
					$rgb_bg_value = hex2rgb($background_colour) ?: "";
					$rgba_bg_string = $rgb_bg_value ? "rgba(" . $rgb_bg_value . ", 90%)" : $background_colour;
					$style .= " --panelBackground: " . $rgba_bg_string . ";";
				endif;
				if($text_colour):
					$style .= " --colorText: " . $text_colour . ";";
				endif;
				if($link_colour):
					$style .= " --colorLink: " . $link_colour . ";";
				endif;
				if($link_hover_colour):
					$style .= " --colorLinkHover: " . $link_hover_colour . ";";
				endif;
				?>
				<div class="swiper-slide swiper-slide--about" style="<?php echo $style; ?>" tabindex="0">
					<div class="swiper-slide-inner">
						<header class="swiper-slide-header">
							<h2><?php echo get_the_title($about_id); ?></h2>
						</header>
						<div class="swiper-no-swiping">
							<?php 
							$content_post = get_post($about_id);
							$content = $content_post->post_content;
							$content = apply_filters('the_content', $content);
							$content = str_replace(']]>', ']]&gt;', $content);
							echo $content;
							wp_reset_postdata(); ?>
						</div>
					</div>
				</div>

				<div class="swiper-slide swiper-slide--empty">
					<?php if(get_field('show_zoom_out_button_on_empty_slide', 'option')): ?>
						<button class="zoom-out text-button" type="button">
							<?php $zoom_image = get_field('zoom_out_image', 'option');
							if($zoom_image): ?>
								<span class="screen-reader-text">Zoom out</span>
								<img src="<?php echo $zoom_image['sizes']['medium']; ?>" loading="lazy" width="<?php echo $zoom_image['sizes']['medium-width']; ?>" height="<?php echo $zoom_image['sizes']['medium-height']; ?>" alt="Zoom out" />
							<?php else: ?>
								<span>Zoom out</span>
							<?php endif; ?>
						</button>
					<?php endif; ?>
				</div>
			<?php endif; ?>

		</div>
	</div>

	<?php
	endwhile; // End of the loop.
	?>


	<div class="idle-container"></div>

<?php
get_footer();
