<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Hear_the_Light
 */
 
$frontpage_id = get_option( 'page_on_front' );
wp_redirect ( get_permalink ($frontpage_id) );
