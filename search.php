<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Hear_the_Light
 */

$frontpage_id = get_option( 'page_on_front' );
wp_redirect ( get_permalink ($frontpage_id) );
